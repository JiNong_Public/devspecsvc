/**
 * @fileoverview device specification service API
 * @author joonyong.jinong@gmail.com
 * @version 1.0.0
 * @since 2019.06.23
 */

/*jshint esversion: 6 */

/**
 * notice
 * 일단 스펙을 제공하기만 함.
 **/

var mysql = require('mysql');
var request = require('request');

var devspec = function () {
    "use strict";

    var _initialized = false;
    var _pool = null;
    var _session = "";
    var _config = {
        "host": "localhost",
        "user": "root",
        "password": "jinong",
        "database": "devspec",
        "timezone": "Asia/Seoul"
    };

    /**
     * @method initialize
     * @description 모듈을 초기화 한다.
     */
    var initialize = function () {
        return new Promise(function (resolve, reject) {
            if (_initialized) {
                resolve();
            } else {
                _pool = mysql.createPool(_config);
                _initialized = true;
                resolve();
            }
        });
    };

    /**
     * @method finalize
     * @description 모듈사용을 종료한다.
     */
    var finalize = function () {
        _pool.end(function (err) {
            if (err)
                console.log("error : " + err);
            else
                _initialized = false;
        });
    };

    /**
     * @method _execute
     * @param {String} query - database query
     * @param {Object} param - parameter for query
     * @return Promise 성공시 true
     * @description query와 param을 받아서 실행한다.
     */
    var _execute = function (query, param) {
        console.log("_execute", query, param);
        return new Promise(function (resolve, reject) {
            _pool.getConnection(function (err, conn) {
                if (err) {
                    console.log("fail to get connection", err);
                    reject(new Error("fail to get db connection"));

                } else {
                    conn.query(query, param, function (error, results, fields) {
                        if (error) {
                            console.log("error : " + query);
                            console.log(error);
                            conn.release();
                            reject(new Error("fail to execute query : " + query));
                        } else {
                            conn.release();
                            resolve(true);
                        }
                    });
                }
            });
        });
    };

    /**
     * @method _executeandresult
     * @param {String} query - database query
     * @param {Object} param - parameter for query
     * @return Promise 성공시 조회결과
     * @description query와 param을 받아서 실행한다.
     */
    var _executeandresult = function (query, param) {
        console.log("_execute&result", query, param);
        return new Promise(function (resolve, reject) {
            _pool.getConnection(function (err, conn) {
                if (err) {
                    console.log("fail to get connection", err);
                    reject(new Error("fail to get db connection"));

                } else {
                    conn.query(query, param, function (error, results, fields) {
                        if (error) {
                            console.log("error : " + query);
                            console.log(error);
                            conn.release();
                            reject(new Error("fail to execute query : " + query));
                        } else {
                            console.log(results);
                            conn.release();
                            resolve(results);
                        }
                    });
                }
            });
        });
    };

    var getdevspecfromKD = function (compcode, code, devtype) {
        return new Promise(function (resolve, reject) {
            var url = "http://file.koreadigital.com/device.asp?compcode=" + compcode + "&code=" + code + "&devtype=" + devtype
            console.log(url);
            request(url, function (error, response, body) {
                if (response && response.statusCode == 200) {
                    console.log(body);
                    var spec = JSON.parse(body);
                    resolve(spec);
                } else {
                    reject(error);
                }
            });
        });
    };

    var getdevspecfromDB = function (compcode, code, devtype, ndtype) {
        return new Promise(function (resolve, reject) {
            _executeandresult("select devspec from devspec where deleted = false and compcode = ? and devcode = ? and devtype = ? and ndtype = ?", [compcode, code, devtype, ndtype])
                .then(function (data) {
                    console.log(data);
                    var spec = JSON.parse(data[0].devspec);
                    console.log(spec);
                    resolve(spec);
                })
                .catch(function (err) {
                    console.log(err);
                    reject(err);
                });
        });
    };


    var getdevspec = function (req, res) {
        var params = req.swagger.params;
        var ndtype = -1

        if (('compcode' in params) &&
            ('code' in params) &&
            ('devtype' in params) && (params.devtype.value)) {

            if (('ndtype' in params) && (params.ndtype.value))
                ndtype = params.ndtype.value

            var spec = {};
            initialize()
                .then(function () {
                    if (params.compcode.value == "8877") {
                        return getdevspecfromKD(params.compcode.value, params.code.value, params.devtype.value);
                    } else {
                        return getdevspecfromDB(params.compcode.value, params.code.value, params.devtype.value, ndtype);
                    }
                })
                .then(function (spec) {
                    res.json(spec);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(405).send(err);
                });
        } else {
            res.status(400).send("There is no devspec.");
        }
    };

    var updatedevspec = function (req, res) {
        var params = req.swagger.params;
        var ndtype = -1

        if (('compcode' in params) &&
            ('code' in params) &&
            ('body' in params) && (params.body.value) &&
            ('devtype' in params) && (params.devtype.value)) {

            if (('ndtype' in params) && (params.ndtype.value))
                ndtype = params.ndtype.value

            //devspec = JSON.stringfy(params.devspec.value);

            var spec = {};
            initialize()
                .then(function () {
                    console.log(params.body.value);
                    spec = JSON.stringify(params.body.value);
                    return _execute("insert into devspec(compcode, devcode, devtype, devspec, ndtype) values (?, ?, ?, ?, ?) on duplicate key update devspec = ?, updated = now()", [params.compcode.value, params.code.value, params.devtype.value, spec, ndtype, spec]);

                })
                .then(function () {
                    res.json(spec);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(405).send(err);
                });
        } else {
            res.status(400).send("There is no devspec.");
        }
    };

    const getAllDevspec = (req, res) => {
        const query = `select * from devspec_test where deleted = false`;
        initialize()
            .then(function () {
                return _executeandresult(query);
            })
            .then(function (spec) {
                res.json(spec);
            })
            .catch(function (err) {
                console.log(err);
                res.status(405).send(err);
            });
    }

    const editDevspec = (req, res) => {
        initialize()
            .then(function () {
                const id = req.swagger.params.id.value;
                const devspecData = req.swagger.params.body.value;

                const query = `UPDATE devspec_test SET compcode = ${devspecData.compcode}, devcode = ${devspecData.devcode}, devspec = '${devspecData.devspec}', devtype = '${devspecData.devtype}', ndtype = ${devspecData.ndtype} WHERE id = ${id}`;
                return _executeandresult(query);
            })
            .then(function (spec) {
                res.json(spec);
            })
            .catch(function (err) {
                console.log(err);
                res.status(405).send(err);
            });
    }

    return {
        getdevspec: getdevspec,
        updatedevspec: updatedevspec,
        getAllDevspec,
        editDevspec
    };
};

module.exports = devspec();

