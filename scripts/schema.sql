CREATE DATABASE devspec DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use devspec;

CREATE TABLE devspec (
	compcode int(11) not null,
	devcode int(11) not null,
	devtype varchar(20) not null,
	deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    devspec text,
	primary key (compcode, devcode, devtype)
);

